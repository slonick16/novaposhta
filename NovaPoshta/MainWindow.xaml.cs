﻿using NovaPoshta.Services;
using RibbonControls.Controls;

namespace NovaPoshta
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : RibbonWindow
    {
        public MainWindow()
        {
            this.InitializeComponent();
            this.DataContext = IoC.MainViewModel;
        }
    }
}