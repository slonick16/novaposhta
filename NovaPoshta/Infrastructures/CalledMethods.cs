﻿namespace NovaPoshta.Infrastructures
{
    public static class CalledMethods
    {
        public const string ActivationLoyalty = "activationLoyalty";
        public const string CalledMethod = "calledMethod";
        public const string CancelPayment = "cancelPayment";
        public const string CheckVerificationCodeForLoyaltyInfo = "checkVerificationCodeForLoyaltyInfo";
        public const string CloneLoyaltyCounterpartySender = "cloneLoyaltyCounterpartySender";
        public const string CreatePaymentOnSite = "CreatePaymentOnSite";
        public const string CreatePaymentOnSiteAlreadyPaid = "CreatePaymentOnSiteAlreadyPaid";
        public const string Delete = "delete";
        public const string DocumentsTracking = "documentsTracking";
        public const string ForgotCard = "forgotCard";
        public const string ForgotPassword = "forgotPassword";
        public const string GetActiveTariffPlans = "getActiveTariffPlans";
        public const string GetActiveTariffPlanData = "getActiveTariffPlanData";
        public const string GetBackwardDeliveryCargoTypes = "getBackwardDeliveryCargoTypes";
        public const string GetCargoDescriptionList = "getCargoDescriptionList";
        public const string GetCargoTypes = "getCargoTypes";
        public const string GetCities = "getCities";
        public const string GetClosedDocumentsByPhone = "getClosedDocumentsByPhone";
        public const string GetCounterparty = "getCounterparties";
        public const string GetCounterpartyAddress = "getCounterpartyAddresses";
        public const string GetDocumentDeliveryDate = "getDocumentDeliveryDate";
        public const string GetDocumentList = "getDocumentList";
        public const string GetDocumentPrice = "getDocumentPrice";
        public const string GetDocumentStatuses = "getDocumentStatuses";
        public const string GetLoyaltyCardTurnoverByApiKey = "getLoyaltyCardTurnoverByApiKey";
        public const string GetLoyaltyUserByApiKey = "getLoyaltyInfoByApiKey";
        public const string GetLoyaltyUserByLogin = "getLoyaltyUserByLogin";
        public const string GetLoyaltyUserByPhone = "getLoyaltyInfoByPhone";
        public const string GetNews = "getNews";
        public const string GetNewsRubrics = "getNewsRubrics";
        public const string GetOwnershipFormsList = "getOwnershipFormsList";
        public const string GetPackList = "getPackList";
        public const string GetPalletsList = "getPalletsList";
        public const string GetPaymentForms = "getPaymentForms";
        public const string GetRecipientPaymentInfo = "getRecipientPaymentInfo";
        public const string GetSenderPaymentInfo = "getSenderPaymentInfo";
        public const string GetServiceTypes = "getServiceTypes";
        public const string GetStatusDocuments = "getStatusDocuments";
        public const string GetStreet = "getStreet";
        public const string GetTimeIntervals = "getTimeIntervals";
        public const string GetTiresWheelsList = "getTiresWheelsList";
        public const string GetTraysList = "getTraysList";
        public const string GetTypesOfCounterparties = "getTypesOfCounterparties";
        public const string GetTypesOfPayers = "getTypesOfPayers";
        public const string GetTypesOfPayersForRedelivery = "getTypesOfPayersForRedelivery";
        public const string GetUnclosedDocumentsByPhone = "getUnclosedDocumentsByPhone";
        public const string GetWarehouses = "getWarehouses";
        public const string GetWarehouseTypes = "getWarehouseTypes";
        public const string Registration = "registration";
        public const string Save = "save";
        public const string SendConsolidateOrder = "sendConsolidateOrder";
        public const string SendFeedback = "sendFeedbackMessageCrm";
        public const string Update = "update";
    }
}