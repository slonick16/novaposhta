﻿namespace NovaPoshta.Infrastructures
{
    /// <summary>
    /// Сортировка накладных
    /// </summary>
    public enum SortInvoice
    {
        /// <summary>
        /// Дата создания
        /// </summary>
        CreationDate,

        /// <summary>
        /// Дата отправки
        /// </summary>
        DepartureDate,

        /// <summary>
        /// Дата доствки
        /// </summary>
        DeliveryDate,

        /// <summary>
        /// Стоимость доставки
        /// </summary>
        CostOfDelivery,

        /// <summary>
        /// Город назначения
        /// </summary>
        CityOfDestination,

        /// <summary>
        /// Статус отправления
        /// </summary>
        DepartureStatus,
    }
}