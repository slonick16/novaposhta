﻿namespace NovaPoshta.Infrastructures
{
    public enum InvoiceFilterCategory
    {
        Number,
        CityOfDeparture,
        CityOfReception,
        SenderName,
        RecipientName
    }
}