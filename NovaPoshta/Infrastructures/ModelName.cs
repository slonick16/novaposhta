﻿namespace NovaPoshta.Infrastructures
{
    public static class ModelName
    {
        public const string Address = "Address";
        public const string Android = "android";
        public const string CityRef = "cityRef";
        public const string Common = "Common";
        public const string CommonGeneral = "CommonGeneral";
        public const string ContactPerson = "ContactPerson";
        public const string CounterParty = "Counterparty";
        public const string DocumentNumber = "document_number";
        public const string InternetDocument = "InternetDocument";
        public const string LoyaltyUser = "LoyaltyUser";
        public const string LoyaltyUserGeneral = "LoyaltyUserGeneral";
        public const string News = "News";
        public const string Phone = "phone";
        public const string Platform = "platform";
        public const string PushLang = "push_lang";
        public const string ScanSheet = "ScanSheet";
        public const string Stage = "stage";
        public const string Token = "token";
        public const string TrackingDocument = "TrackingDocument";
    }
}