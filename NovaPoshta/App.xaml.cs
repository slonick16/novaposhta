﻿using System;
using System.Threading.Tasks;
using RibbonControls.Extensions;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using NovaPoshta.Helpers;
using NovaPoshta.Infrastructures;
using NovaPoshta.Services;
using NovaPoshta.Services.Logging;
using RibbonControls.Controls;
using RibbonControls.Helpers;

namespace NovaPoshta
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            Current.DispatcherUnhandledException += this.UnhandledException;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            if (this.TryFindResource("AccentColorBrush") is SolidColorBrush accentBrush)
            {
                accentBrush.Color.CreateAccentColors();
            }

            var splashWindow = new SplashWindow
            {
                AppName = "Нова Пошта",
                WindowStartupLocation = WindowStartupLocation.CenterScreen
            };
            splashWindow.Closed += (sender, args) =>
            {
                if (!this.MainWindow?.IsVisible ?? false)
                {
                    Shutdown(0);
                }
            };
            splashWindow.Show();

            //Start Init
            if (ConnectionHelper.CheckInternet() != ConnectionHelper.ConnectionStatus.Connected)
            {
                NotificationBox.Show("Відсутнє підключення до інтернету.", "Помилка", MessageBoxButton.OK,
                    NotificationBox.NotificationBoxImage.Error);
                splashWindow.Close();
                return;
            }

            ApplicationSetup();

            //End Init

            Task.Delay(500).ContinueWith((_) =>
            {
                UIHelper.Execute(() =>
                {
                    this.MainWindow = new MainWindow();
                    this.MainWindow.Show();
                    splashWindow.Close();
                });
            });
        }

        /// <summary>
        /// Configures our application ready for use
        /// </summary>
        private static async void ApplicationSetup()
        {
            // Setup IoC
            IoC.Setup();

            await IoC.NovaPoshtaService.Init();
            await IoC.MainViewModel.Init();

#if !DEBUG
            IoC.LogFactory.IncludeLogOriginDetails = false;
            IoC.LogFactory.LogOutputLevel = LogOutputLevel.Informative;
#endif

            IoC.LogFactory.AddLogger(
                new FileLogger($"{PathHelper.LogFolder}\\{DateTime.Now:yy-MM-dd}-{DateTime.Now:hh-mm-ss}.log"));
        }

        private void UnhandledException(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            IoC.LogFactory.Log(e.Exception.Message, LogLevel.Error);
            IoC.LogFactory.Log(e.Exception.StackTrace, LogLevel.Error);

            e.Handled = true;
        }
    }
}