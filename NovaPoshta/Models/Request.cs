﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    [KnownType(typeof(Document[]))]
    public class Request
    {
        [DataMember(Name = "apiKey")] public string ApiKey { get; set; }

        [DataMember(Name = "modelName")] public string ModelName { get; set; }

        [DataMember(Name = "calledMethod")] public string CalledMethod { get; set; }

        [DataMember(Name = "methodProperties")]
        public Dictionary<string, object> MethodProperties { get; set; }
    }
}