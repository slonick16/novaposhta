﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    public class Response<T> where T : class
    {
        [DataMember(Name = "success")] public bool Success { get; set; }
        [DataMember(Name = "data")] public T[] Data { get; set; }
        [DataMember(Name = "errors")] public string[] Errors { get; set; }
        [DataMember(Name = "warnings")] public List<Dictionary<string, string>> Warnings { get; set; }
        [DataMember(Name = "info")] public string[] Info { get; set; }
        [DataMember(Name = "messageCodes")] public string[] MessageCodes { get; set; }
        [DataMember(Name = "errorCodes")] public string[] ErrorCodes { get; set; }
        [DataMember(Name = "warningCodes")] public string[] WarningCodes { get; set; }
        [DataMember(Name = "infoCodes")] public string[] InfoCodes { get; set; }
    }
}