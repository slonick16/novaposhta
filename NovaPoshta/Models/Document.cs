﻿using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    public class Document
    {
        [DataMember] public string Barcode { get; set; }
        [DataMember] public string DataType { get; set; }
    }
}