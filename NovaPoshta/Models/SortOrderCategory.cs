﻿using System.Data.SqlClient;
using NovaPoshta.Infrastructures;

namespace NovaPoshta.Models
{
    public class SortOrderCategory
    {
        public string Name { get; set; }
        public SortInvoice Value { get; set; }
    }
}