﻿using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    public class StatusDocument
    {
        [DataMember] public string DocumentNumber { get; set; }
        [DataMember] public string Phone { get; set; }
    }
}