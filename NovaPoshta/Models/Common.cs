﻿using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    public class Common
    {
        [DataMember] public string Description { get; set; }
        [DataMember] public string Ref { get; set; }
    }
}