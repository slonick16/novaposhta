﻿using System.Runtime.Serialization;

namespace NovaPoshta.Models
{
    [DataContract]
    public class UserProfile
    {
        [DataMember] public string ApiKey { get; set; }
        [DataMember] public string LoyaltyCardNumber { get; set; }
        [DataMember] public string FullName { get; set; }
        [DataMember] public string Discount { get; set; }
        [DataMember] public string City { get; set; }
        [DataMember] public ulong Phone { get; set; }
        [DataMember] public string BirthDate { get; set; }
        [DataMember] public string Email { get; set; }
        [DataMember] public string CounterpartyRef { get; set; }
    }
}