﻿using System.Data.SqlClient;
using System.Runtime.Serialization;
using NovaPoshta.Infrastructures;

namespace NovaPoshta.Models
{
    [DataContract]
    public class Settings
    {
        [DataMember] public bool ShowUnclosedDocuments { get; set; }

        [DataMember] public bool ShowClosedDocuments { get; set; }

        [DataMember] public bool ShowUsersDocuments { get; set; }

        [DataMember] public SortInvoice SortInvoice { get; set; }

        [DataMember] public SortOrder SortOrder { get; set; }
    }
}