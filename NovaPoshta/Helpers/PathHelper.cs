﻿using System;
using System.IO;

namespace NovaPoshta.Helpers
{
    public static class PathHelper
    {
        public static string UserDocumentFolder => Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

        private static string AppFolder => CreateIfNoExist(Path.Combine(UserDocumentFolder, "NovaPoshta"));

        public static string LogFolder => CreateIfNoExist(Path.Combine(AppFolder, "Logs"));

        public static string SettingsFile => Path.Combine(AppFolder, "Settings.json");

        public static string UserProfileFile => Path.Combine(AppFolder, "UserProfile.json");

        public static string CargoTypesFile => Path.Combine(AppFolder, "CargoTypes.json");

        public static string TypesOfPayersFile => Path.Combine(AppFolder, "TypesOfPayers.json");

        public static string PaymentFormsFile => Path.Combine(AppFolder, "PaymentForms.json");

        public static string TypesOfPayersForRedeliveryFile =>
            Path.Combine(AppFolder, "TypesOfPayersForRedelivery.json");

        public static string ServiceTypesFile => Path.Combine(AppFolder, "ServiceTypes.json");

        public static string OwnershipFormsListFile => Path.Combine(AppFolder, "OwnershipFormsList.json");

        private static string CreateIfNoExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            return path;
        }
    }
}