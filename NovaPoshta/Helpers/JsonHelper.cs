﻿using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Web.Script.Serialization;

namespace NovaPoshta.Helpers
{
    public static class JsonHelper
    {
        public static string Serialize(object obj)
        {
            using (var memoryStream = new MemoryStream())
            {
                var settings = new DataContractJsonSerializerSettings {UseSimpleDictionaryFormat = true};
                var serializer = new DataContractJsonSerializer(obj.GetType(), settings);
                serializer.WriteObject(memoryStream, obj);
                var json = Encoding.UTF8.GetString(memoryStream.ToArray());
                return json;
            }
        }

        public static T Deserialize<T>(string json)
        {
            var serializer = new JavaScriptSerializer();
            return serializer.Deserialize<T>(json);
        }
    }
}