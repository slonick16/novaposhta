﻿using System.IO;
using System.Net;

namespace NovaPoshta.Helpers
{
    public class ConnectionHelper
    {
        public enum ConnectionStatus
        {
            NotConnected,
            LimitedAccess,
            Connected
        }

        public static ConnectionStatus CheckInternet()
        {
            try
            {
                var entry = Dns.GetHostEntry("dns.msftncsi.com");
                if (entry.AddressList.Length == 0)
                {
                    return ConnectionStatus.NotConnected;
                }

                if (!entry.AddressList[0].ToString().Equals("131.107.255.255"))
                {
                    return ConnectionStatus.LimitedAccess;
                }
            }
            catch
            {
                return ConnectionStatus.NotConnected;
            }

            var request = (HttpWebRequest) WebRequest.Create("http://www.msftncsi.com/ncsi.txt");
            try
            {
                var response = (HttpWebResponse) request.GetResponse();

                if (response.StatusCode != HttpStatusCode.OK)
                {
                    return ConnectionStatus.LimitedAccess;
                }

                using (var sr = new StreamReader(response.GetResponseStream()))
                {
                    return sr.ReadToEnd().Equals("Microsoft NCSI")
                        ? ConnectionStatus.Connected
                        : ConnectionStatus.LimitedAccess;
                }
            }
            catch
            {
                return ConnectionStatus.NotConnected;
            }
        }
    }
}