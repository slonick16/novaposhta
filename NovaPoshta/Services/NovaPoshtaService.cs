﻿using Newtonsoft.Json;
using NovaPoshta.Helpers;
using NovaPoshta.Infrastructures;
using NovaPoshta.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using RibbonControls.Controls;

namespace NovaPoshta.Services
{
    public class NovaPoshtaService
    {
        #region Fields

        private readonly Uri _apiUrl = new Uri("https://api.novaposhta.ua/v2.0/json/");

        private List<Common> _cargoTypes;
        private List<Common> _typesOfPayers;
        private List<Common> _paymentForms;
        private readonly List<Common> _typesOfPayersForRedelivery;
        private readonly List<Common> _serviceTypes;
        private readonly List<Common> _ownershipFormsList;

        #endregion Fields

        #region Public Methods

        public async Task<Document[]> GetClosedDocumentsByPhone()
        {
            var curDate = DateTime.Now.AddMonths(1);
            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.LoyaltyUser,
                CalledMethod = CalledMethods.GetClosedDocumentsByPhone,
                MethodProperties = new Dictionary<string, object>
                {
                    {MethodProperties.Phone, "380957954574"},
                    {MethodProperties.LoyaltyUserMonth, curDate.Month.ToString()},
                    {MethodProperties.LoyaltyUserYear, curDate.Year.ToString()}
                }
            };

            var documents = await this.MakeRequest<Document>(request);
            return documents;
        }

        public async Task<Document[]> GetUnclosedDocumentsByPhone()
        {
            var curDate = DateTime.Now.AddMonths(1);
            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.LoyaltyUser,
                CalledMethod = CalledMethods.GetUnclosedDocumentsByPhone,
                MethodProperties = new Dictionary<string, object>
                {
                    {MethodProperties.Phone, "380957954574"},
                    {MethodProperties.LoyaltyUserMonth, curDate.Month.ToString()},
                    {MethodProperties.LoyaltyUserYear, curDate.Year.ToString()}
                }
            };

            var documents = await this.MakeRequest<Document>(request);
            return documents;
        }

        public async Task<TrackingDocument[]> GetStatusDocuments(Document[] documents)
        {
            if (documents == null)
            {
                return null;
            }

            var request = new Request
            {
                ModelName = "TrackingDocument",
                CalledMethod = "getStatusDocuments",
                MethodProperties = new Dictionary<string, object>
                {
                    {
                        MethodProperties.Documents,
                        documents.Select(d => new StatusDocument
                        {
                            DocumentNumber = d.Barcode,
                            Phone = "380957954574",
                        })
                    },
                    {MethodProperties.Language, MethodProperties.Ua}
                }
            };

            var document = await this.MakeRequest<TrackingDocument>(request);
            return document;
        }

        public async Task<UserProfile> GetLoyaltyUserByLogin(string login, string password)
        {
            var request = new Request
            {
                ModelName = ModelName.LoyaltyUser,
                CalledMethod = CalledMethods.GetLoyaltyUserByLogin,
                MethodProperties = new Dictionary<string, object>
                {
                    {MethodProperties.Login, login},
                    {MethodProperties.Password, password}
                }
            };

            var userProfiles = await this.MakeRequest<UserProfile>(request);
            return userProfiles?.First();
        }

        public string GetCargoType(string refValue) =>
            this._cargoTypes.FirstOrDefault(c => c.Ref == refValue)?.Description ?? refValue;

        public string GetTypesOfPayer(string refValue) =>
            this._typesOfPayers.FirstOrDefault(c => c.Ref == refValue)?.Description ?? refValue;

        public string GetPaymentForm(string refValue) =>
            this._paymentForms.FirstOrDefault(c => c.Ref == refValue)?.Description ?? refValue;

        public async Task Init()
        {
            await this.GetCargoTypes();
            await this.GetTypesOfPayers();
            await this.GetPaymentForms();
            //await this.GetTypesOfPayersForRedelivery();
            //await this.GetServiceTypes();
            //await this.GetOwnershipFormsList();
        }

        #endregion Public Methods

        #region Private Methods

        private async Task GetCargoTypes()
        {
            if (File.Exists(PathHelper.CargoTypesFile))
            {
                var creationDate = File.GetCreationTime(PathHelper.CargoTypesFile);
                if (creationDate.Month - DateTime.Now.Month < 1)
                {
                    this._cargoTypes =
                        JsonConvert.DeserializeObject<List<Common>>(File.ReadAllText(PathHelper.CargoTypesFile));
                    return;
                }
            }

            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetCargoTypes,
                MethodProperties = new Dictionary<string, object>()
            };

            var cargoTypes = await this.MakeRequest<Common>(request);
            File.WriteAllText(PathHelper.CargoTypesFile, JsonConvert.SerializeObject(cargoTypes));
            this._cargoTypes = cargoTypes.ToList();
        }

        private async Task GetTypesOfPayers()
        {
            if (File.Exists(PathHelper.TypesOfPayersFile))
            {
                var creationDate = File.GetCreationTime(PathHelper.TypesOfPayersFile);
                if (creationDate.Month - DateTime.Now.Month < 1)
                {
                    this._typesOfPayers =
                        JsonConvert.DeserializeObject<List<Common>>(File.ReadAllText(PathHelper.TypesOfPayersFile));
                    return;
                }
            }

            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetTypesOfPayers,
                MethodProperties = new Dictionary<string, object>()
            };

            var typesOfPayersFile = await this.MakeRequest<Common>(request);
            File.WriteAllText(PathHelper.TypesOfPayersFile, JsonConvert.SerializeObject(typesOfPayersFile));
            this._typesOfPayers = typesOfPayersFile.ToList();
        }

        private async Task GetPaymentForms()
        {
            if (File.Exists(PathHelper.PaymentFormsFile))
            {
                var creationDate = File.GetCreationTime(PathHelper.PaymentFormsFile);
                if (creationDate.Month - DateTime.Now.Month < 1)
                {
                    this._paymentForms =
                        JsonConvert.DeserializeObject<List<Common>>(File.ReadAllText(PathHelper.PaymentFormsFile));
                    return;
                }
            }

            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetPaymentForms,
                MethodProperties = new Dictionary<string, object>()
            };

            var paymentForms = await this.MakeRequest<Common>(request);
            File.WriteAllText(PathHelper.PaymentFormsFile, JsonConvert.SerializeObject(paymentForms));
            this._paymentForms = paymentForms.ToList();
        }

        private async Task<Common[]> GetTypesOfPayersForRedelivery()
        {
            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetTypesOfPayersForRedelivery,
                MethodProperties = new Dictionary<string, object>()
            };

            var documents = await this.MakeRequest<Common>(request);
            return documents;
        }

        private async Task<Common[]> GetServiceTypes()
        {
            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetServiceTypes,
                MethodProperties = new Dictionary<string, object>()
            };

            var documents = await this.MakeRequest<Common>(request);
            return documents;
        }

        private async Task<Common[]> GetOwnershipFormsList()
        {
            var request = new Request
            {
                ApiKey = IoC.MainViewModel.UserProfile.ApiKey,
                ModelName = ModelName.Common,
                CalledMethod = CalledMethods.GetOwnershipFormsList,
                MethodProperties = new Dictionary<string, object>()
            };

            var documents = await this.MakeRequest<Common>(request);
            return documents;
        }

        private async Task<T[]> MakeRequest<T>(Request request) where T : class
        {
            using (var client = new HttpClient())
            {
                if (ConnectionHelper.CheckInternet() != ConnectionHelper.ConnectionStatus.Connected)
                {
                    NotificationBox.Show("Відсутнє підключення до інтернету.", "Помилка", MessageBoxButton.OK,
                        NotificationBox.NotificationBoxImage.Error);
                    return null;
                }

                var json = JsonConvert.SerializeObject(request);
                var requestContent = new StringContent(json, Encoding.UTF8, "application/json");

                IoC.LogFactory.Log(json, LogLevel.Debug);

                var response = await client.PostAsync(this._apiUrl, requestContent).ConfigureAwait(false);
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }

                var content = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                var responseContent = JsonConvert.DeserializeObject<Response<T>>(Regex.Unescape(content));
                return responseContent.Success ? responseContent.Data : null;
            }
        }

        #endregion
    }
}