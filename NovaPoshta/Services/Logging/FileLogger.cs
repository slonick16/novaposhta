﻿using System;
using System.IO;
using NovaPoshta.Infrastructures;
using NovaPoshta.Interfaces;

namespace NovaPoshta.Services.Logging
{
    public class FileLogger : ILogger
    {
        #region Public Properties

        /// <summary>
        /// The path to write the log file to
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// If true, logs the current time with each message
        /// </summary>
        public bool LogTime { get; set; } = true;

        #endregion

        #region Constructor

        /// <summary>
        /// Default constructor
        /// </summary>
        /// <param name="filePath">The path to log to</param>
        public FileLogger(string filePath)
        {
            // Set the file path property
            this.FilePath = filePath;
        }

        #endregion

        #region Logger Methods

        public void Log(string message, LogLevel level)
        {
            // The default category
            var category = default(string);

            // Color console based on level
            switch (level)
            {
                // Debug
                case LogLevel.Debug:
                    category = "D:";
                    break;

                //Informative
                case LogLevel.Informative:
                    category = "I:";
                    break;

                // Verbose
                case LogLevel.Verbose:
                    category = "V:";
                    break;

                // Warning
                case LogLevel.Warning:
                    category = "W:";
                    break;

                // Error
                case LogLevel.Error:
                    category = "E:";
                    break;

                // Success
                case LogLevel.Success:
                    category = "-----";
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(level), level, null);
            }

            // Get current time
            var currentTime = DateTimeOffset.Now.ToString("yyyy-MM-dd hh:mm:ss");

            // Prepend the time to the log if desired
            var timeLogString = this.LogTime ? $"[{currentTime}] " : "";

            // Write the message
            using (var file = File.AppendText(this.FilePath))
            {
                file.WriteLine($"{timeLogString}{category}{message}");
            }
        }

        #endregion
    }
}