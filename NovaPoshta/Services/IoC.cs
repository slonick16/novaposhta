﻿using Ninject;
using NovaPoshta.Interfaces;
using NovaPoshta.Services.Logging;
using NovaPoshta.ViewModels;

namespace NovaPoshta.Services
{
    /// <summary>
    /// The IoC container for our application
    /// </summary>
    public static class IoC
    {
        #region Public Properties

        /// <summary>
        /// The kernel for our IoC container
        /// </summary>
        private static IKernel Kernel { get; } = new StandardKernel();

        public static ILogFactory LogFactory => Get<ILogFactory>();

        public static NovaPoshtaService NovaPoshtaService => Get<NovaPoshtaService>();

        public static MainViewModel MainViewModel => Get<MainViewModel>();

        #endregion

        #region Construction

        /// <summary>
        /// Sets up the IoC container, binds all information required and is ready for use
        /// NOTE: Must be called as soon as your application starts up to ensure all 
        ///       services can be found
        /// </summary>
        public static void Setup()
        {
            // Bind all required view models
            BindViewModels();
        }

        /// <summary>
        /// Binds all singleton view models
        /// </summary>
        private static void BindViewModels()
        {
            // Bind to a single instance of Application view model
            Kernel.Bind<MainViewModel>().ToConstant(new MainViewModel());
            Kernel.Bind<ILogFactory>().ToConstant(new BaseLogFactory());
            Kernel.Bind<NovaPoshtaService>().ToConstant(new NovaPoshtaService());
        }

        #endregion

        /// <summary>
        /// Get's a service from the IoC, of the specified type
        /// </summary>
        /// <typeparam name="T">The type to get</typeparam>
        /// <returns></returns>
        public static T Get<T>() => Kernel.Get<T>();
    }
}