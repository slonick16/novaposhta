﻿using System.Threading.Tasks;

namespace NovaPoshta.Extensions
{
    public static class TaskExtension
    {
        public static void RunTaskSynchronously(this Task t)
        {
            var task = Task.Run(async () => await t);
            task.Wait();
        }

        public static T RunTaskSynchronously<T>(this Task<T> t)
        {
            return Task.Run(async () => await t).GetAwaiter().GetResult();
        }
    }
}