﻿using System;
using NovaPoshta.Models;
using NovaPoshta.Services;
using RibbonControls.ViewModel.Base;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Newtonsoft.Json;
using NovaPoshta.Helpers;
using NovaPoshta.Infrastructures;
using RibbonControls.Controls;
using RibbonControls.Infrastructures;

namespace NovaPoshta.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        private TrackingDocument _selectedDocument;
        private UserProfile _userProfile;
        private string _login;
        private bool _isOpenMenu;
        private bool _isBusy;
        private string _title;
        private bool _showUnclosedDocuments = true;
        private bool _showClosedDocuments = true;
        private bool _showUsersDocuments = true;
        private SortInvoice _sortInvoice = SortInvoice.CreationDate;
        private SortOrder _sortOrder = SortOrder.Ascending;
        private Dictionary<string, SortInvoice> _sortOrderCategories;
        private bool _isOpenSortMenu;
        private string _invoiceFilter;
        private InvoiceFilterCategory _invoiceFilterCategory;

        public string Title
        {
            get => _title;
            set => this.Set(ref _title, value);
        }

        public string InvoiceFilter
        {
            get => _invoiceFilter;
            set
            {
                if (this.Set(ref _invoiceFilter, value))
                {
                    this.RefreshShowedItems();
                }
            }
        }

        public InvoiceFilterCategory InvoiceFilterCategory
        {
            get => _invoiceFilterCategory;
            set
            {
                if (this.Set(ref _invoiceFilterCategory, value))
                {
                    this.RefreshShowedItems();
                }
            }
        }

        public SortInvoice SortInvoice
        {
            get => _sortInvoice;
            set => this.Set(ref _sortInvoice, value);
        }

        public SortOrder SortOrder
        {
            get => _sortOrder;
            set => this.Set(ref _sortOrder, value);
        }

        public UserProfile UserProfile
        {
            get => _userProfile;
            set => this.Set(ref _userProfile, value);
        }

        public bool ShowUnclosedDocuments
        {
            get => _showUnclosedDocuments;
            set
            {
                this.Set(ref _showUnclosedDocuments, value);
                this.RefreshShowedItems();
                this.SaveSettings();
            }
        }

        public bool ShowClosedDocuments
        {
            get => _showClosedDocuments;
            set
            {
                this.Set(ref _showClosedDocuments, value);
                this.RefreshShowedItems();
                this.SaveSettings();
            }
        }

        public bool ShowUsersDocuments
        {
            get => _showUsersDocuments;
            set
            {
                this.Set(ref _showUsersDocuments, value);
                this.RefreshShowedItems();
                this.SaveSettings();
            }
        }

        public bool IsBusy
        {
            get => _isBusy;
            set => this.Set(ref _isBusy, value);
        }

        public bool IsOpenSortMenu
        {
            get => _isOpenSortMenu;
            set => this.Set(ref _isOpenSortMenu, value);
        }

        public bool IsOpenMenu
        {
            get => _isOpenMenu;
            set
            {
                if (this.UserProfile != null)
                {
                    this.Set(ref _isOpenMenu, value);
                    return;
                }

                this.Set(ref _isOpenMenu, true);
            }
        }

        public string Login
        {
            get => _login;
            set => this.Set(ref _login, value);
        }

        private List<TrackingDocument> UnclosedDocuments { get; set; }

        private List<TrackingDocument> ClosedDocuments { get; set; }

        private List<TrackingDocument> UsersDocuments { get; set; }

        public ObservableCollection<TrackingDocument> DisplayDocuments { get; set; }

        public TrackingDocument SelectedDocument
        {
            get => this._selectedDocument;
            set => this.Set(ref this._selectedDocument, value);
        }

        public ICommand LoginCommand { get; set; }
        public ICommand LogoutCommand { get; set; }
        public ICommand CopyToClipboardCommand { get; set; }
        public ICommand RefreshCommand { get; set; }
        public ICommand SortCommand { get; set; }
        public ICommand SortOrderCommand { get; set; }

        public MainViewModel()
        {
            this.Title = "Нова Пошта";

            this.LoginCommand = new RelayCommand<PasswordBox>(LoginExecute, LoginCanExecute);
            this.LogoutCommand = new RelayCommand(LogoutExecute);
            this.CopyToClipboardCommand = new RelayCommand<ulong>(CopyToClipboardExecute);
            this.RefreshCommand = new RelayCommand(RefreshExecute);
            this.SortCommand = new RelayCommand<SortInvoice>(SortExecute);
            this.SortOrderCommand = new RelayCommand(SortOrderExecute);
        }

        private void SortOrderExecute()
        {
            this.SortOrder = this.SortOrder == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
            this.RefreshShowedItems();
            this.SaveSettings();
        }

        private void SortExecute(SortInvoice sortInvoice)
        {
            this.SortInvoice = sortInvoice;
            this.RefreshShowedItems();
            this.SaveSettings();
            this.IsOpenSortMenu = false;
        }

        private async void RefreshExecute()
        {
            var npService = IoC.NovaPoshtaService;

            this.IsBusy = true;

            var unclosedDocuments = await npService.GetUnclosedDocumentsByPhone();
            var unclosedStatusDocuments = await npService.GetStatusDocuments(unclosedDocuments);
            this.UnclosedDocuments = unclosedStatusDocuments?.ToList();

            var closedDocuments = await npService.GetClosedDocumentsByPhone();
            var closedStatusDocuments = await npService.GetStatusDocuments(closedDocuments);
            this.ClosedDocuments = closedStatusDocuments?.ToList();

            this.RefreshShowedItems();

            this.IsBusy = false;
        }

        private void RefreshShowedItems()
        {
            var displayDocuments = new List<TrackingDocument>();
            if (this.ShowUnclosedDocuments && this.UnclosedDocuments != null)
            {
                displayDocuments = displayDocuments.Concat(this.UnclosedDocuments).ToList();
            }

            if (this.ShowClosedDocuments && this.ClosedDocuments != null)
            {
                displayDocuments = displayDocuments.Concat(this.ClosedDocuments).ToList();
            }

            if (this.ShowUsersDocuments && this.UsersDocuments != null)
            {
                displayDocuments = displayDocuments.Concat(this.UsersDocuments).ToList();
            }

            displayDocuments = this.Filter(displayDocuments).ToList();

            this.DisplayDocuments = new ObservableCollection<TrackingDocument>(this.Sort(displayDocuments));

            this.RaisePropertyChanged(nameof(this.DisplayDocuments));
        }

        private IEnumerable<TrackingDocument> Filter(IEnumerable<TrackingDocument> displayDocuments)
        {
            if (string.IsNullOrWhiteSpace(this.InvoiceFilter))
            {
                return displayDocuments;
            }

            switch (this.InvoiceFilterCategory)
            {
                case InvoiceFilterCategory.Number:
                    return displayDocuments.Where(d => d.Number.ToString().StartsWith(this.InvoiceFilter));
                case InvoiceFilterCategory.CityOfDeparture:
                    return displayDocuments.Where(d => d.CitySender.StartsWith(this.InvoiceFilter));
                case InvoiceFilterCategory.CityOfReception:
                    return displayDocuments.Where(d => d.CityRecipient.StartsWith(this.InvoiceFilter));
                case InvoiceFilterCategory.SenderName:
                    return displayDocuments.Where(d => d.SenderFullNameEW.Contains(this.InvoiceFilter));
                case InvoiceFilterCategory.RecipientName:
                    return displayDocuments.Where(d => d.RecipientFullName.Contains(this.InvoiceFilter));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IEnumerable<TrackingDocument> Sort(List<TrackingDocument> trackingDocuments)
        {
            switch (this.SortInvoice)
            {
                case SortInvoice.CreationDate:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => DateTime.Parse(t.DateCreated))
                        : trackingDocuments?.OrderByDescending(t => DateTime.Parse(t.DateCreated));
                case SortInvoice.DepartureDate:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => DateTime.Parse(t.DateCreated))
                        : trackingDocuments?.OrderByDescending(t => DateTime.Parse(t.DateCreated));
                case SortInvoice.DeliveryDate:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => DateTime.Parse(t.ScheduledDeliveryDate))
                        : trackingDocuments?.OrderByDescending(t => DateTime.Parse(t.ScheduledDeliveryDate));
                case SortInvoice.CostOfDelivery:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => t.DocumentCost)
                        : trackingDocuments?.OrderByDescending(t => t.DocumentCost);
                case SortInvoice.CityOfDestination:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => t.CityRecipient)
                        : trackingDocuments?.OrderByDescending(t => t.CityRecipient);
                case SortInvoice.DepartureStatus:
                    return this.SortOrder == SortOrder.Ascending
                        ? trackingDocuments?.OrderBy(t => t.StatusCode)
                        : trackingDocuments?.OrderByDescending(t => t.StatusCode);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void CopyToClipboardExecute(ulong number)
        {
            Clipboard.SetDataObject(number.ToString());
        }

        private void LogoutExecute()
        {
            File.Delete(PathHelper.UserProfileFile);
            this.Title = "Нова Пошта";
            this.UserProfile = null;
            this.UnclosedDocuments?.Clear();
            this.SelectedDocument = null;
        }

        private async void LoginExecute(PasswordBox passwordBox)
        {
            var userProfile = await IoC.NovaPoshtaService.GetLoyaltyUserByLogin(this.Login, passwordBox.Password);

            if (userProfile is null)
            {
                NotificationBox.Show("Невірна електронна пошта або пароль.", "Помилка", MessageBoxButton.OK,
                    NotificationBox.NotificationBoxImage.Error);
                return;
            }

            File.WriteAllText(PathHelper.UserProfileFile, JsonConvert.SerializeObject(userProfile));
            this.UserProfile = userProfile;

            this.Title = $"{this.UserProfile.FullName} - Нова Пошта";

            this.Login = string.Empty;
            passwordBox.Password = string.Empty;

            this.IsOpenMenu = false;
            this.IsBusy = true;

            await this.Init();
            await IoC.NovaPoshtaService.Init();
            await Task.Delay(5000);

            this.IsBusy = false;
        }

        private bool LoginCanExecute(PasswordBox passwordBox)
        {
            return !string.IsNullOrWhiteSpace(this.Login) && !string.IsNullOrWhiteSpace(passwordBox.Password);
        }

        public async Task Init()
        {
            if (!File.Exists(PathHelper.UserProfileFile))
            {
                this.IsOpenMenu = true;
                return;
            }

            this.UserProfile = JsonConvert.DeserializeObject<UserProfile>(File.ReadAllText(PathHelper.UserProfileFile));

            this.Title = $"{this.UserProfile.FullName} - Нова Пошта";

            var npService = IoC.NovaPoshtaService;

            var unclosedDocuments = await npService.GetUnclosedDocumentsByPhone();
            var unclosedStatusDocuments = await npService.GetStatusDocuments(unclosedDocuments);
            this.UnclosedDocuments = unclosedStatusDocuments?.ToList();

            var closedDocuments = await npService.GetClosedDocumentsByPhone();
            var closedStatusDocuments = await npService.GetStatusDocuments(closedDocuments);
            this.ClosedDocuments = closedStatusDocuments?.ToList();

            this.RestoreSettings();

            this.RefreshShowedItems();
        }

        private void SaveSettings()
        {
            var settings = new Settings
            {
                ShowClosedDocuments = this.ShowClosedDocuments,
                ShowUnclosedDocuments = this.ShowUnclosedDocuments,
                ShowUsersDocuments = this.ShowUsersDocuments,
                SortInvoice = this.SortInvoice,
                SortOrder = this.SortOrder
            };

            File.WriteAllText(PathHelper.SettingsFile, JsonConvert.SerializeObject(settings));
        }

        private void RestoreSettings()
        {
            if (!File.Exists(PathHelper.SettingsFile))
            {
                return;
            }

            var settings = JsonConvert.DeserializeObject<Settings>(File.ReadAllText(PathHelper.SettingsFile));
            this._showClosedDocuments = settings.ShowClosedDocuments;
            this._showUnclosedDocuments = settings.ShowUnclosedDocuments;
            this._showUsersDocuments = settings.ShowUsersDocuments;
            this._sortInvoice = settings.SortInvoice;
            this._sortOrder = settings.SortOrder;

            this.RaisePropertyChanged(nameof(this.ShowClosedDocuments));
            this.RaisePropertyChanged(nameof(this.ShowUnclosedDocuments));
            this.RaisePropertyChanged(nameof(this.ShowUsersDocuments));
            this.RaisePropertyChanged(nameof(this.SortInvoice));
            this.RaisePropertyChanged(nameof(this.SortOrder));
        }
    }
}