﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace NovaPoshta.Controls
{
    /// <summary>
    /// A control featuring a range of loading indicating animations.
    /// </summary>
    [TemplatePart(Name = "Border", Type = typeof(Border))]
    public class BusyIndicator : ContentControl
    {
        /// <summary>
        /// Identifies the <see cref="BusyIndicator.SpeedRatio"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SpeedRatioProperty =
            DependencyProperty.Register("SpeedRatio", typeof(double), typeof(BusyIndicator), new PropertyMetadata(1d,
                (o, e) =>
                {
                    BusyIndicator li = (BusyIndicator) o;

                    if (li.PART_Border == null || li.IsBusy == false)
                    {
                        return;
                    }

                    foreach (VisualStateGroup group in VisualStateManager.GetVisualStateGroups(li.PART_Border))
                    {
                        if (group.Name == "ActiveStates")
                        {
                            foreach (VisualState state in group.States)
                            {
                                if (state.Name == "Active")
                                {
                                    state.Storyboard.SetSpeedRatio(li.PART_Border, (double) e.NewValue);
                                }
                            }
                        }
                    }
                }));

        /// <summary>
        /// Identifies the <see cref="BusyIndicator.IsBusy"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsBusyProperty =
            DependencyProperty.Register("IsBusy", typeof(bool), typeof(BusyIndicator), new PropertyMetadata(true,
                (o, e) =>
                {
                    BusyIndicator li = (BusyIndicator) o;

                    if (li.PART_Border == null || DesignerProperties.GetIsInDesignMode(li))
                    {
                        return;
                    }

                    if ((bool) e.NewValue == false)
                    {
                        VisualStateManager.GoToElementState(li.PART_Border, "Inactive", false);
                        li.PART_Border.Visibility = Visibility.Collapsed;
                    }
                    else
                    {
                        VisualStateManager.GoToElementState(li.PART_Border, "Active", false);
                        li.PART_Border.Visibility = Visibility.Visible;

                        foreach (VisualStateGroup group in VisualStateManager.GetVisualStateGroups(li.PART_Border))
                        {
                            if (group.Name == "ActiveStates")
                            {
                                foreach (VisualState state in group.States)
                                {
                                    if (state.Name == "Active")
                                    {
                                        state.Storyboard.SetSpeedRatio(li.PART_Border, li.SpeedRatio);
                                    }
                                }
                            }
                        }
                    }
                }));

        // Variables
        private Border PART_Border;

        /// <summary>
        /// Get/set the speed ratio of the animation.
        /// </summary>
        public double SpeedRatio
        {
            get => (double) this.GetValue(SpeedRatioProperty);
            set => this.SetValue(SpeedRatioProperty, value);
        }

        /// <summary>
        /// Get/set whether the loading indicator is active.
        /// </summary>
        public bool IsBusy
        {
            get => (bool) this.GetValue(IsBusyProperty);
            set => this.SetValue(IsBusyProperty, value);
        }

        /// <summary>
        /// When overridden in a derived class, is invoked whenever application code
        /// or internal processes call System.Windows.FrameworkElement.ApplyTemplate().
        /// </summary>
        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            this.PART_Border = (Border) this.GetTemplateChild("PART_Border");

            if (this.PART_Border != null)
            {
                VisualStateManager.GoToElementState(this.PART_Border, (this.IsBusy ? "Active" : "Inactive"), false);
                foreach (VisualStateGroup group in VisualStateManager.GetVisualStateGroups(this.PART_Border))
                {
                    if (group.Name == "ActiveStates")
                    {
                        foreach (VisualState state in group.States)
                        {
                            if (state.Name == "Active")
                            {
                                state.Storyboard.SetSpeedRatio(this.PART_Border, this.SpeedRatio);
                            }
                        }
                    }
                }

                this.PART_Border.Visibility = (this.IsBusy ? Visibility.Visible : Visibility.Collapsed);
            }
        }
    }
}