﻿using NovaPoshta.Services;
using RibbonControls.ValueConverter;
using System;
using System.Globalization;

namespace NovaPoshta.Converters
{
    public class PayerTypeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string valueAsString && !string.IsNullOrWhiteSpace(valueAsString))
            {
                return IoC.NovaPoshtaService.GetTypesOfPayer(valueAsString);
            }

            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();
    }
}