﻿using NovaPoshta.Services;
using RibbonControls.ValueConverter;
using System;
using System.Globalization;

namespace NovaPoshta.Converters
{
    public class CargoTypeConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string valueAsString && !string.IsNullOrWhiteSpace(valueAsString))
            {
                return IoC.NovaPoshtaService.GetCargoType(valueAsString);
            }

            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();
    }
}