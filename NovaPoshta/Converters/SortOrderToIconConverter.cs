﻿using RibbonControls.Converters;
using System;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows;

namespace NovaPoshta.Converters
{
    public class SortOrderToIconConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SortOrder sortOrder)
            {
                if (sortOrder == SortOrder.Ascending)
                {
                    return Application.Current.Resources["SortUpImage"];
                }

                return Application.Current.Resources["SortDownImage"];
            }

            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}