﻿using RibbonControls.ValueConverter;
using System;
using System.Globalization;

namespace NovaPoshta.Converters
{
    public class SplitStringConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is string valueAsString && !string.IsNullOrWhiteSpace(valueAsString))
            {
                if (parameter is string parameterAsString && int.TryParse(parameterAsString, out var index))
                {
                    var splitedValue = valueAsString.Split(new[] {" "}, StringSplitOptions.RemoveEmptyEntries);
                    if (splitedValue.Length > index)
                    {
                        return splitedValue[index];
                    }
                }
            }

            return value;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();
    }
}