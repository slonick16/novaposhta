﻿using RibbonControls.ValueConverter;
using System;
using System.Globalization;

namespace NovaPoshta.Converters
{
    public class StringToDateConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return DateTime.TryParse(value.ToString(), out var date) ? date.ToString("dd.MM.yyyy") : value.ToString().Replace('-', '.');
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            throw new NotImplementedException();
    }
}