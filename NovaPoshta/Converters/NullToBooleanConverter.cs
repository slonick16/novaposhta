﻿using RibbonControls.ValueConverter;
using System;
using System.Globalization;

namespace NovaPoshta.Converters
{
    public class NullToBooleanConverter : BaseValueConverter
    {
        public override object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (parameter == null)
            {
                return value == null;
            }

            return value != null;
        }

        public override object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }
}